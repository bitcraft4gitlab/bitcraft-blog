<?php namespace Bitcraft\BitcraftBlog\Behaviors;

use DOMDocument;
use DOMXPath;
use Illuminate\Support\Facades\DB;
use RainLab\Translate\Models\Locale;

class QuicklinkBehavior extends \October\Rain\Extension\ExtensionBase
{
    protected $parent;
    protected $disk;

    public function __construct($parent)
    {
        $this->parent = $parent;
    }

    public function setQuicklinks(): void
    {
        $quicklinks = [];
        if (!$modules = $this->parent->modules) {
            return;
        }
        $locales = array_keys( Locale::listAvailable());
        foreach ($locales as $locale) {
            $this->parent->translateContext($locale);
            foreach ($modules as $index => $module) {
                if (array_key_exists('data', $module) && $module['_group'] === 'text_module') {
                    $content = $module['data']['content'];
                    $dom = new DOMDocument();
                    $dom->loadHTML('<?xml encoding="utf-8" ?>' . $content);

                    foreach ($dom->getElementsByTagName('p') as $element) {
                        $element->removeAttribute('id');
                    }

                    $finder = new DomXPath($dom);
                    $classname="is-quicklink";
                    $elements = $finder->query("//*[contains(@class, '$classname')]");
                    foreach ($elements as $inner => $element) {
                        $id = str_slug($element->nodeValue);
                        $quicklinks[$id] = $element->nodeValue;
                        $elements->item($inner)->setAttribute('id', $id);
                    }
                    $html= str_replace(
                        [
                            '<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">',
                            '</body></html>',
                            '<html><body>',
                            '<?xml encoding="utf-8" ?>'
                        ], '', $dom->saveHTML());
                    $modules[$index]['data']['content'] = $html;
                }
            }
            if($locale == Locale::getDefault()->code) {
                DB::table($this->parent->table)
                    ->where('id', $this->parent->id)
                    ->update(
                        [
                            'modules' => json_encode($modules),
                            'quicklinks' => json_encode($quicklinks)
                        ]
                    );
                continue;
            }

            if($data = DB::table('rainlab_translate_attributes')
                ->where('model_type', get_class($this))
                ->where('model_id', $this->parent->id)
                ->where('locale', $locale)
                ->first()) {
                $data = json_decode($data->attribute_data);
                $data->modules = $modules;
                $data->quicklinks = $quicklinks;

                DB::table('rainlab_translate_attributes')
                    ->where('model_type', get_class($this))
                    ->where('model_id', $this->parent->id)
                    ->where('locale', $locale)
                    ->update(['attribute_data' => json_encode($data)]);
            }
        }
    }

}
