<?php namespace Bitcraft\BitcraftBlog\Console;

use Bitcraft\BitcraftBlog\Jobs\PublishPostsWithDate;
use Bitcraft\Dogbible\Models\BlogPost;
use Bitcraft\Dogbible\Models\Breed;
use Bitcraft\Dogbible\Models\Image;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Queue;
use Renatio\SeoManager\Models\SeoTag;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class PushPublishJob extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'bitcraftblog:push_publish_job';

    /**
     * @var string The console command description.
     */
    protected $description = 'Push publish job to queue';

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        Queue::push(PublishPostsWithDate::class);
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }
}
