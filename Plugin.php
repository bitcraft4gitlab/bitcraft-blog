<?php namespace Bitcraft\BitcraftBlog;

use Bitcraft\BitcraftBlog\Console\PushPublishJob;
use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
    }

    public function registerSettings()
    {
    }

    public function register()
    {
        $this->registerConsoleCommand('bitcraftblog.push_publish_job', PushPublishJob::class);
    }

    public function registerSchedule($schedule)
    {
        // $schedule->command('dogbible:night_jobs')->dailyAt('03:00');
        $schedule->command('bitcraftblog:push_publish_job')->everyTenMinutes();
    }
}
