<?php namespace Bitcraft\BitcraftBlog\Jobs;

use Bitcraft\BitcraftBlog\Models\BlogPost;

class PublishPostsWithDate
{
    public $timeout = 99999;
    public function fire($job, $data)
    {
        $posts = BlogPost::all();
        foreach ($posts as $post) {
            if (!empty($post->publish_at) && strtotime($post->publish_at) < time()) {
                $post->published = true;
                $post->published_at = date('Y-m-d H:i:s');
                $post->save();
            }
        }
        $job->delete();
    }
}
