<?php namespace Bitcraft\BitcraftBlog\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class BlogAuthors extends Controller
{
    public $implement = [
        'Backend\Behaviors\ListController',
        'Backend\Behaviors\FormController',
        'Backend\Behaviors\RelationController'
    ];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Bitcraft.BitcraftBlog', 'main-menu-item-blog', 'side-menu-item-authors');
    }
}
