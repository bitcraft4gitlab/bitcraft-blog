<?php namespace Bitcraft\BitcraftBlog\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBitcraftBitcraftblogBlogPosts3 extends Migration
{
    public function up()
    {
        Schema::table('bitcraft_bitcraftblog_blog_posts', function($table)
        {
            $table->integer('author_id')->nullable()->unsigned();
        });
    }
    
    public function down()
    {
        Schema::table('bitcraft_bitcraftblog_blog_posts', function($table)
        {
            $table->dropColumn('author_id');
        });
    }
}
