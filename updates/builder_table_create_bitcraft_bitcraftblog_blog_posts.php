<?php namespace Bitcraft\BitcraftBlog\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateBitcraftBitcraftblogBlogPosts extends Migration
{
    public function up()
    {
        Schema::create('bitcraft_bitcraftblog_blog_posts', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->string('title');
            $table->string('slug');
            $table->string('banner')->nullable();
            $table->text('banner_alt')->nullable();
            $table->text('banner_title')->nullable();
            $table->string('banner_size')->nullable();
            $table->text('modules')->nullable();
            $table->boolean('published')->nullable();
            $table->dateTime('publish_at')->nullable();
            $table->timestamp('published_at')->nullable();
            $table->integer('category_id')->nullable()->unsigned();
            $table->text('short_description')->nullable();
            $table->integer('reading_time')->nullable()->unsigned();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('bitcraft_bitcraftblog_blog_posts');
    }
}
