<?php namespace Bitcraft\BitcraftBlog\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBitcraftBitcraftblogBlogCategories3 extends Migration
{
    public function up()
    {
        Schema::table('bitcraft_bitcraftblog_blog_categories', function($table)
        {
            $table->string('banner_color')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('bitcraft_bitcraftblog_blog_categories', function($table)
        {
            $table->dropColumn('banner_color');
        });
    }
}
