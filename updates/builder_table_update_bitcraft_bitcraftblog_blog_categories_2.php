<?php namespace Bitcraft\BitcraftBlog\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBitcraftBitcraftblogBlogCategories2 extends Migration
{
    public function up()
    {
        Schema::table('bitcraft_bitcraftblog_blog_categories', function($table)
        {
            $table->text('short_description')->nullable();
            $table->string('banner')->nullable();
            $table->text('banner_alt')->nullable();
            $table->text('banner_title')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('bitcraft_bitcraftblog_blog_categories', function($table)
        {
            $table->dropColumn('short_description');
            $table->dropColumn('banner');
            $table->dropColumn('banner_alt');
            $table->dropColumn('banner_title');
        });
    }
}
