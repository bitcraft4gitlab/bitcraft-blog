<?php namespace Bitcraft\BitcraftBlog\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBitcraftBitcraftblogBlogPosts extends Migration
{
    public function up()
    {
        Schema::table('bitcraft_bitcraftblog_blog_posts', function($table)
        {
            $table->text('quicklinks')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('bitcraft_bitcraftblog_blog_posts', function($table)
        {
            $table->dropColumn('quicklinks');
        });
    }
}
