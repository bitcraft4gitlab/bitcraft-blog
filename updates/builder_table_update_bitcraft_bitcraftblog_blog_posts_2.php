<?php namespace Bitcraft\BitcraftBlog\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBitcraftBitcraftblogBlogPosts2 extends Migration
{
    public function up()
    {
        Schema::table('bitcraft_bitcraftblog_blog_posts', function($table)
        {
            $table->boolean('featured')->default(false);
        });
    }
    
    public function down()
    {
        Schema::table('bitcraft_bitcraftblog_blog_posts', function($table)
        {
            $table->dropColumn('featured');
        });
    }
}
