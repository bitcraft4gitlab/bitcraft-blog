<?php namespace Bitcraft\BitcraftBlog\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateBitcraftBitcraftblogBlogAuthors extends Migration
{
    public function up()
    {
        Schema::create('bitcraft_bitcraftblog_blog_authors', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->string('title');
            $table->text('description')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('bitcraft_bitcraftblog_blog_authors');
    }
}
