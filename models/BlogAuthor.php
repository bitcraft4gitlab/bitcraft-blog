<?php namespace Bitcraft\BitcraftBlog\Models;

use Model;

/**
 * Model
 */
class BlogAuthor extends Model
{
    use \October\Rain\Database\Traits\Validation;

    public $implement = [
        'RainLab.Translate.Behaviors.TranslatableModel'
    ];

    public $translatable = ['description'];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'bitcraft_bitcraftblog_blog_authors';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $hidden = [
        'created_at',
        'updated_at',
        'description'
    ];

    public $morphOne = [
        'seo_tag' => [
            'Bitcraft\Seomanager\Models\SeoTag',
            'table' => 'bitcraft_seomanager_seo_tags',
            'name' => 'seo_tag'
        ]
    ];

    /**
     * Return a nice seo title for this category.
     *
     * @return string
     */
    public function title()
    {
        if (@$this->seo_tag->title) {
            return $this->seo_tag->title;
        }

        return $this->title;
    }

    /**
     * Return a clean seo description for this category.
     *
     * @param int $max_length
     * @return null|string
     */
    public function description($max_length = 156)
    {
        $desc = null;
        if (@$this->seo_tag->description) {
            return trim(substr(str_replace(
                ['"', "'", '“', '”'],
                '',
                strip_tags($this->seo_tag->description)
            ), 0, $max_length));
        }

        $desc = $this->description;

        if ($desc) {
            $desc = html_entity_decode($desc);
            return trim(substr(str_replace(['"', "'", '“', '”'], '', strip_tags($desc)), 0, $max_length));
        }

        return null;
    }
}
