<?php namespace Bitcraft\BitcraftBlog\Models;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Model;
use RainLab\Translate\Models\Locale;
use System\Classes\MediaLibrary;

/**
 * Model
 */
class BlogPost extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use \October\Rain\Database\Traits\Sluggable;

    public $implement = [
        'RainLab.Translate.Behaviors.TranslatableModel',
        'Bitcraft.BitcraftBlog.Behaviors.QuicklinkBehavior'
    ];
    protected $jsonable = ['modules', 'quicklinks'];
    public $translatable = [
        'modules',
        'short_description',
        'title',
        'quicklinks',
        'slug',
        'banner_alt',
        'banner_title'
    ];
    protected $slugs = ['slug' => 'title'];
    protected $appends = ['banner_path', 'updated'];
    public $hidden = [
        'created_at',
        'updated_at',
        'published',
        'publish_at',
        'featured'
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'bitcraft_bitcraftblog_blog_posts';

    /**
     * @var array Validation rules
     */
    public $rules = [
        'title' => 'required',
        'slug' => 'regex:/^[0-9a-zA-z\/\-\_]+$/',
    ];

    public $morphOne = [
        'seo_tag' => [
            'Bitcraft\Seomanager\Models\SeoTag',
            'table' => 'bitcraft_seomanager_seo_tags',
            'name' => 'seo_tag'
        ]
    ];

    public $belongsTo = [
        'category' => BlogCategory::class,
        'author' => BlogAuthor::class
    ];

    public function getBannerPathAttribute()
    {
        return MediaLibrary::url($this->banner);
    }

    public function getUpdatedAttribute()
    {
        return !empty($this->updated_at) ? $this->updated_at->format('d/m/Y') : '';
    }

    public function beforeSave()
    {
        if ($this->published && $this->published_at == null) {
            $this->published_at = Carbon::now();
        } elseif (!$this->published) {
            $this->published_at = null;
        }
    }

    public function frontendPath($locale)
    {
        $slug = $this->lang($locale)->slug;
        if ($mappedLocale = config("app.locales.$locale")) {
            return env('FRONTEND_URL')."/$mappedLocale/blog/$slug";
        }
        return env('FRONTEND_URL')."/$locale/blog/$slug";
    }

    public function afterSave()
    {
        $this->setQuicklinks();
        $this->setReadingTime();
    }

    public function scopePublished($query)
    {
        return $query->where('published', true);
    }

    public function scopeNew($query)
    {
        return $query->orderBy('published_at', 'desc');
    }

    public function scopeFeatured($query)
    {
        return $query->where('featured', 1);
    }

    public function scopeBySlugWithLocale($query, $slug, $locale)
    {
        return $query->transWhere('slug', $slug, substr($locale, 0, 2));
    }

    public function scopeReduced($query)
    {
        return $query->select([
            'title',
            'slug',
            'published_at',
            'short_description',
            'banner',
            'banner_title',
            'banner_alt',
            'category_id',
            'author_id'
        ]);
    }

    public function calculateReadingTime($text)
    {
        $word_count = str_word_count($text);
        return (int)floor($word_count / 150);
    }

    public function setReadingTime(): void
    {
        if (!$modules = $this->modules) {
            return;
        }
        $locales = array_keys(Locale::listAvailable());

        foreach ($locales as $locale) {
            $this->translateContext($locale);
            $all_words_in_post = '';
            foreach ($modules as $index => $module) {
                if (array_key_exists('data', $module) && $module['_group'] === 'text_module') {
                    $all_words_in_post .= $module['data']['content'];
                }
            }
            $reading_time = $this->calculateReadingTime($all_words_in_post);
            if ($locale == Locale::getDefault()->code) {
                DB::table($this->table)
                    ->where('id', $this->id)
                    ->update(
                        [
                            'reading_time' => $reading_time
                        ]
                    );
                continue;
            }

            if ($data = DB::table('rainlab_translate_attributes')
                ->where('model_type', get_class($this))
                ->where('model_id', $this->id)
                ->where('locale', $locale)
                ->first()) {
                $data = json_decode($data->attribute_data);
                $data->reading_time = $reading_time;

                DB::table('rainlab_translate_attributes')
                    ->where('model_type', get_class($this))
                    ->where('model_id', $this->id)
                    ->where('locale', $locale)
                    ->update(['attribute_data' => json_encode($data)]);
            }
        }
    }

    /**
     * Return a nice seo title for this post.
     *
     * @return string
     */
    public function title()
    {
        if (@$this->seo_tag->title) {
            return $this->seo_tag->title;
        }

        return $this->title;
    }

    /**
     * Return a clean seo description for this post.
     *
     * @param int $max_length
     * @return null|string
     */
    public function description($max_length = 156)
    {
        $desc = null;
        if (@$this->seo_tag->description) {
            return trim(substr(str_replace(
                ['"', "'", '“', '”'],
                '',
                strip_tags($this->seo_tag->description)
            ), 0, $max_length));
        }

        $desc = $this->short_description;

        if ($desc) {
            $desc = html_entity_decode($desc);
            return trim(substr(str_replace(['"', "'", '“', '”'], '', strip_tags($desc)), 0, $max_length));
        }

        return null;
    }
}
