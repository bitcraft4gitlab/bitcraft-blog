<?php namespace Bitcraft\BitcraftBlog\Models;

use Model;
use System\Classes\MediaLibrary;

/**
 * Model
 */
class BlogCategory extends Model
{
    use \October\Rain\Database\Traits\Validation;

    public $implement = [
        'RainLab.Translate.Behaviors.TranslatableModel'
    ];
    protected $slugs = ['slug' => 'title'];

    public $translatable = ['title', 'description', 'slug', 'short_description', 'banner_alt', 'banner_title'];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'bitcraft_bitcraftblog_blog_categories';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $hidden = [
        'created_at',
        'updated_at'
    ];

    public $hasMany = [
        'posts' => [
            BlogPost::class,
            'key' => 'category_id'
        ]
    ];

    public $morphOne = [
        'seo_tag' => [
            'Bitcraft\Seomanager\Models\SeoTag',
            'table' => 'bitcraft_seomanager_seo_tags',
            'name' => 'seo_tag'
        ]
    ];

    protected $appends = ['banner_path'];

    public function getBannerPathAttribute()
    {
        return MediaLibrary::url($this->banner);
    }

    public function scopeBySlugWithLocale($query, $slug, $locale)
    {
        return $query->transWhere('slug', $slug, substr($locale, 0, 2));
    }

    /**
     * Return a nice seo title for this category.
     *
     * @return string
     */
    public function title()
    {
        if (@$this->seo_tag->title) {
            return $this->seo_tag->title;
        }

        return $this->title;
    }

    /**
     * Return a clean seo description for this category.
     *
     * @param int $max_length
     * @return null|string
     */
    public function description($max_length = 156)
    {
        $desc = null;
        if (@$this->seo_tag->description) {
            return trim(substr(str_replace(
                ['"', "'", '“', '”'],
                '',
                strip_tags($this->seo_tag->description)
            ), 0, $max_length));
        }

        $desc = $this->short_description;

        if ($desc) {
            $desc = html_entity_decode($desc);
            return trim(substr(str_replace(['"', "'", '“', '”'], '', strip_tags($desc)), 0, $max_length));
        }

        return null;
    }
}
